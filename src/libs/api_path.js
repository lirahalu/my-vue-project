/**
 * Created by yaojiewei on 2017/10/30.
 */
import env from '../../build/env';

//登录权限模块
const ssoUrl = env === 'development'
    ? '/sso/'//开发环境地址 已起代理
    : env === 'production'
        ? '/'
        : 'https://debug.url.com';

//其他模块
const apiUrl = env === 'development'
    ? '/api/'//开发环境地址 已起代理
    : env === 'production'
        ? '/'
        : 'https://debug.url.com';

const utilUrl = env === 'development'
    ? '/api/'//开发环境地址 已起代理
    : env === 'production'
        ? '/'
        : 'https://debug.url.com';

const srcUrl = env === 'development'
    ? '/src/'//开发环境地址 已起代理
    : env === 'production'
        ? '/'
        : 'https://debug.url.com';

//短信模块
const smsUrl = env === 'development'
    ? '/sms/'//开发环境地址 已起代理
    : env === 'production'
        ? '/message/'
        : 'https://debug.url.com';

//微信模块
const wxUrl = env === 'development'
    ? '/wx/'//开发环境地址 已起代理
    : env === 'production'
        ? '/'
        : 'https://debug.url.com';

const connentUrl = env === 'development'
    ? 'http://dev.yhlng.com/message/'//开发环境地址 已起代理
    : env === 'production'
        ? '/message/'
        : 'https://debug.url.com';

//接口地址
const path = {
    upload:apiUrl+'manage/upload/upload',//上传附件接口

    importCompany:ssoUrl+'manage/system/company/import',//导入公司
    importStation:ssoUrl+'manage/system/station/import',//导入站点


    login: ssoUrl + 'sso/login',//用户登录
    logout: ssoUrl + 'sso/logout',//用户登出
    reg: ssoUrl + 'sso/reg',//用户注册
    associateUser: ssoUrl + 'sso/open/associateUser',//用户注册

    sendRegisterSms: smsUrl + 'sms/verificationCode/register',//发送注册验证码

    /*日志管理*/
    getLogList: ssoUrl + "manage/system/log/list",//日志列表
    delLog: ssoUrl + "manage/system/log/delete",//删除日志

    /*用户行为分析*/
    monthCounts: ssoUrl + "manage/system/behavior/month/counts",//每月用户增长量
    companyPercent: ssoUrl + "manage/system/behavior/company/user/percent",//不同类型公司的用户占比
    modulePercent: ssoUrl + "manage/system/behavior/module/user/percent",//不同模块访问占比

    /*组织管理*/
    OrganizationCreate: ssoUrl + "manage/organization/create",//新增组织
    OrganizationDelete: ssoUrl + "manage/organization/delete",//删除组织
    OrganizationList: ssoUrl + "manage/organization/list",//组织列表
    OrganizationUpdate: ssoUrl + "manage/organization/update",//修改组织

    /*机构管理*/
    addCompany: ssoUrl + "manage/system/company/create",//增加机构
    getCompanyInfo: ssoUrl + "manage/system/company/detail",//获取机构详情
    getCompanyList: ssoUrl + "manage/system/company/list",//获取机构列表
    deleteCompany: ssoUrl + "manage/system/company/delete",//删除机构
    getRelationCompanyList: ssoUrl + "manage/system/company/relationCompanyList",//关联机构列表
    updateCompany: ssoUrl + "manage/system/company/update",//修改机构
    getStationListByCompany: ssoUrl + "manage/system/company/station/vehicle/list",//公司下面的站点和车辆列表
    getRelatedListByCompany: ssoUrl + "manage/system/company/relatedList",//公司下面的相关列表（用户、车辆、站点）

    /*权限管理*/
    createPermission: ssoUrl + "manage/system/permission/create",//新增权限
    deletePermission: ssoUrl + "manage/system/permission/delete",//删除权限
    PermissionList: ssoUrl + "manage/system/permission/list",//权限列表
    PermissionRole: ssoUrl + "manage/system/permission/role",//角色权限列表
    PermissionUpdate: ssoUrl + "manage/system/permission/update",//修改权限
    PermissionUser: ssoUrl + "manage/system/permission/user",//用户权限列表
    /*角色管理*/
    createRole: ssoUrl + "manage/system/role/create",//新增角色
    deleteRole: ssoUrl + "manage/system/role/delete",//删除角色
    listRole: ssoUrl + "manage/system/role/list",//角色列表
    permissionRole: ssoUrl + "manage/system/role/permission",//角色权限
    updateRole: ssoUrl + "manage/system/role/update",//修改角色

    /*用户管理*/
    createUser: ssoUrl + "manage/system/user/create",//新增用户
    deleteUser: ssoUrl + "manage/system/user/delete",//删除用户
    listUser: ssoUrl + "manage/system/user/list",//用户列表
    organizationUser: ssoUrl + "manage/system/user/organization",//用户组织
    permissionUser: ssoUrl + "manage/system/user/permission",//用户权限
    roleUser: ssoUrl + "manage/system/user/role",//用户角色
    updateUser: ssoUrl + "manage/system/user/update",//修改用户
    organizationByUser: ssoUrl + "manage/system/user/organization/{id}",//修改用户

    getUserPermission: ssoUrl+"manage/system/v2/permission/user",//获取用户权限

    select: apiUrl + 'manage/widget/select/station',

    /*配送管理*/
    //delivery-require-controller : 需求订单管理
    getRequireOrderList: apiUrl + 'require/getOrderList',//需求订单列表
    addRequireOrder: apiUrl + 'require/addOrder',//新建需求订单
    getRequireOrderDetail: apiUrl + 'require/getDetail',//查看订单详情
    delRequireOrder: apiUrl + 'require/delOrder',//删除订单
    modRequireOrder: apiUrl + 'require/modifyOrder',//修改订单

    getBusinessOrderList: apiUrl + 'businessOrder/getOrderList',//业务订单列表
    addBusinessOrder: apiUrl + 'businessOrder/addBusinessOrder',//新建业务订单
    getBusinessOrderDetail: apiUrl + 'businessOrder/getOrderDetail',//查看业务订单详情
    cancelBusinessOrder: apiUrl + 'businessOrder/cancelOrder',//删除业务订单

    getCarList: apiUrl + 'car/getCarList',//槽车管理列表
    addCar: apiUrl + 'car/addCar',//获取站点详情
    modifyCar: apiUrl + 'car/modifyCar',//获取站点详情
    deleteCar: apiUrl + 'car/deleteCar',//删除槽车
    getCarDetail: apiUrl + 'car/getCarDetail',//删除槽车

    /*设备管理接口*/
    getOnlineEquipList: apiUrl + 'manage/equip/base/OnlineEquip/list',//获取在线设备列表
    addOnlineEquip: apiUrl + 'manage/equip/base/onlineEquip/add',//新增上线设备
    modOnlineEquip: apiUrl + 'manage/equip/base/onlineEquip/update',//修改在线设备
    changeOnlineEquip: apiUrl + 'manage/equip/base/onlineEquip/change',//更换在线设备
    getAddOnlineEquip: apiUrl + 'manage/equip/base/onlineEquip/add',//获取在线设备新增前数据

    getBasicEquipList: apiUrl + 'manage/equip/base/basicEquip/list',//获取设备列表
    addBasicEquip: apiUrl + 'manage/equip/base/basicEquip/add',//新增基础设备
    deleteBasicEquip: apiUrl + 'manage/equip/base/basicEquip/delete',//删除基础设备
    modBasicEquip: apiUrl + 'manage/equip/base/basicEquip/update',//修改基础设备
    getEquipType: apiUrl + 'manage/equip/base/basicEquip/widget',//获取设备类型列表
    getDiscardDetail: apiUrl + 'manage/equip/base/basicEquip/getDiscardDetailByCode',//废弃详情

    getEquipInspectList: apiUrl + 'manage/equip/equipInspect/setting/add',//获取巡检设置和记录新增前数据
    addEquipInspect: apiUrl + 'manage/equip/equipInspect/setting/add',//新增巡检设置
    getEquipInspect: apiUrl + 'manage/equip/equipInspect/setting/list',//获取巡检设置列表
    modEquipInspect: apiUrl + 'manage/equip/equipInspect/setting/update',//修改巡检设置

    getRecordList: apiUrl + 'manage/equip/equipInspect/record/list',//获取巡检记录列表
    addRecord: apiUrl + 'manage/equip/equipInspect/record/add',//新增巡检记录
    modRecord: apiUrl + 'manage/equip/equipInspect/record/update',//修改巡检记录

    getFaultList: apiUrl + 'manage/equip/fault/list',//故障单列表查看
    addFault: apiUrl + 'manage/equip/fault/add',//新增故障单
    cancelFault: apiUrl + 'manage/equip/fault/cancel',//取消故障单
    deleteFault: apiUrl + 'manage/equip/fault/delete',//删除故障单
    detailFault: apiUrl + 'manage/equip/fault/detail',//查看详情
    publishFault: apiUrl + 'manage/equip/fault/publish',//发布故障单

    getLinkMan: apiUrl + 'manage/system/station/getStationUsers',//根据站id/角色查询关联的联系人信息
    getCompany: apiUrl + 'manage/widget/getCompanyByStationId',//根据站点id查询第三方维修公司列表
    getCompanyId: apiUrl + 'manage/widget/getCompanyByUserId',//根据用户id查询所在公司id

    handleResultInner: apiUrl + 'manage/equip/fault/handleResultInner',//加气站处理内部维修故障单（气站内部录入处理结果）
    handleResultThird: apiUrl + 'manage/equip/fault/handleResultThird',//加气站处理第三方维修公司故障单（待确认）


    updateFault: apiUrl + 'manage/equip/fault/vieFault',//第三方维修公司抢单
    vieFaultList: apiUrl + 'manage/equip/fault/vieFaultList',//抢单列表页面查询故障单
    handleFault: apiUrl + 'manage/equip/fault/handleFault',//第三方维修公司处理


    /*站点相关*/
    getLeaderList: apiUrl + 'manage/system/station/leaderList',//获取站长列表
    getStationList: apiUrl + 'manage/system/station/list',//获取站点列表
    getAddStation: apiUrl + 'manage/system/station/add',//新增站点前数据（公司）
    addStation: apiUrl + 'manage/system/station/add',//新增站点
    updateStation: apiUrl + 'manage/system/station/update',//修改站点
    deleteStation: apiUrl + 'manage/system/station/delete',//删除站点
    getStationDetail: apiUrl + 'manage/system/station/getDetail',//获取站点详情
    addStationUser: apiUrl + 'manage/system/station/addStationUser',//新增站点员工前数据
    getStationUsers: apiUrl + 'manage/system/station/getStationUsers',//查看站点现有员工
    bundingBox: apiUrl + 'manage/system/station/bundingBox',//绑定LngBox
    getStationPort: apiUrl + 'manage/system/station/stationPort',//站点报表
    export:apiUrl+'manage/stationPort/export',//导出站点报表接口
    getStationTradeList:apiUrl+'manage/system/station/trade/list',//站点交易列表
    addTradePrice:apiUrl+'manage/system/station/trade/priceAdd',//站点交易单价新增
    getTradePriceList:apiUrl+'manage/system/station/trade/priceList',//站点交易单价列表



    /*监控预警*/
    getRealTimeData: apiUrl + 'manage/hbase/getRealTimeData',//获取实时数据
    getHistoryData: apiUrl + 'manage/hbase/getHistoryData',//获取历史数据
    getRealTimeDataByAreaCode: apiUrl + 'manage/hbase/getRealTimeDataByAreaCode',//hbase区域实时数据
    exportHistoryData:apiUrl+'manage/historyData/export',//导出站点报表接口

    getAlarmInfoList: utilUrl + 'manage/monitor/alarm/getAlarmInfoList',//获取报警信息列表
    getAlarmDetail: utilUrl + 'manage/monitor/alarm/getAlarmDetail',//获取报警信息详情
    handleAlarmInfo: utilUrl + 'manage/monitor/alarm/handleAlarmInfo',//处理报警信息
    getStationsByDistrictCode: apiUrl + 'manage/widget/getStationsByDistrictCode',//获取站点

    getRoutinePersonList: utilUrl + 'manage/monitor/routine/person/list',//获取个人巡检列表
    getRoutineStationList: utilUrl + 'manage/monitor/routine/routine/list',//获取站点巡检列表
    getRoutinePersonDetail: utilUrl + 'manage/monitor/routine/personRoutine/detail',//获取个人巡检详情
    modifyRoutine: utilUrl + 'manage/monitor/routine/modifyRoutine',//修改常规巡检
    getStationRoutineDetail: utilUrl + 'manage/monitor/routine/stationRoutine/detail',//获取站点巡检详情
    addRoutine: utilUrl + 'manage/monitor/routine/addRoutine',//录入常规巡检
    getRoutineProject: utilUrl + 'manage/monitor/routine/getRoutineProject',//获取常规巡检项目

    getShiftInfoList: utilUrl + 'manage/monitor/handover/getShiftInfoList',//获取交接班巡检列表
    getShiftDetail: utilUrl + 'manage/monitor/handover/getShiftDetail',//获取交接班巡检详情
    addShiftReport: utilUrl + 'manage/monitor/handover/addShiftReport',//录入交班报告
    addSuccessionReport: utilUrl + 'manage/monitor/handover/addSuccessionReport',//录入接班报告
    modifyShift: utilUrl + 'manage/monitor/handover/modifyShift', //修改交接班
    getUserList: utilUrl + 'manage/monitor/handover/getUserList',//获取交接班巡检人员
    getInspectTeam: utilUrl + 'manage/monitor/routine/getInspectTeam',//得到所有巡检项


    /*LNG BOX管理*/
    getSmartBoxList: apiUrl + 'manage/smartBox/smartBox/list',//获取box列表
    addLngBox: apiUrl + 'manage/smartBox/smartBox/add',//新增box信息
    deleteLngBox: apiUrl + 'manage/smartBox/smartBox/delete',//删除box信息
    updateLngBox: apiUrl + 'manage/smartBox/smartBox/update',//修改box信息
    getStationUnbound: apiUrl + 'manage/system/station/unbound',//某公司下所有未绑定box的站点或车辆

    /*LNG BOX参数设置*/
    getBoxParameterList: apiUrl + 'manage/smartBox/boxParameter/list',//box参数列表
    addBoxParameter: apiUrl + 'manage/smartBox/boxParameter/add',//新增box参数
    deleteBoxParameter: apiUrl + 'manage/smartBox/boxParameter/delete',//删除box参数
    updateBoxParameter: apiUrl + 'manage/smartBox/boxParameter/update',//修改box参数
    getAddBoxParameterList: apiUrl + 'manage/smartBox/boxParameter/add',//获取box新增前数据
    getAddBoxParameterPort: apiUrl + 'manage/smartBox/boxParameter/port/add',//根据传感器类型查询采集端口

    /*LNGBOX更新*/
    getSmartBoxPush: apiUrl + 'manage/smartBox/version/push',//下发指定到Kafka
    getUpdateList: apiUrl + 'manage/smartBox/version/list',//智能box版本更新列表
    boxUpdate: apiUrl + 'manage/smartBox/version/update',//box更新
    boxConnect: smsUrl + 'sse/boxConnect',//更新推送通道连接
    connect: connentUrl + 'sse/connect',//推送通道连接

    /*触发器*/
    getTriggerList: apiUrl + 'manage/smartBox/trigger/list',//获取触发器list
    getAddTriggerList: apiUrl + 'manage/smartBox/trigger/add',//获取触发器新增前数据
    addTrigger: apiUrl + 'manage/smartBox/trigger/add',//新增触发器
    deleteTrigger: apiUrl + 'manage/smartBox/trigger/delete',//删除触发器
    updateTrigger: apiUrl + 'manage/smartBox/trigger/update',//修改触发器
    getAddTriggerSensor: apiUrl + 'manage/smartBox/trigger/sensor/add',//根据boxId和区域id获取传感器
    getAddTriggerArea: apiUrl + 'manage/smartBox/trigger/area/add',//根据boxId获取区域

    /*下拉*/
    getSelectStation: apiUrl + 'manage/widget/select/station',//获取站点查询
    getStationsByCompanyId: apiUrl + 'manage/widget/getStationsByCompanyId',//根据公司id得到站点列表
    getStationsByCityCode: apiUrl + 'manage/widget/getStationsByLocation',//根据省市区得到站点列表
    getCompanyUserByCompanyId: apiUrl + 'manage/widget/getCompanyUserByCompanyId',//根据公司ID获取该公司下的员工信息
    getStationsByUserId: apiUrl + 'manage/widget/getStationsByUserId',//根据公司ID获取该公司下的员工信息
    getCompanyByUserId: apiUrl + 'manage/widget/v2/getCompanyByUserId',//根据公司ID获取该公司下的员工信息

    /*统计分析*/
    getAlarmCount: apiUrl + 'manage/statistics/getAlarmCount',//气站预警统计
    getAnalysisByProvinceCode: apiUrl + 'manage/statistics/getAnalysisByProvinceCode',//省份消耗分析
    getBeReplacedEquip: apiUrl + 'manage/statistics/getBeReplacedEquip',//待更换设备统计
    getConsumeByLocation: apiUrl + 'manage/statistics/getConsumeByLocation',//气站消耗统计(按照地区分组)
    getConsumeByTime: apiUrl + 'manage/statistics/getConsumeByTime',//气站消耗统计(按照时间分组)
    getStationCountByLocation: apiUrl + 'manage/statistics/getStationCountByLocation',//站点分布
    getStationReserves: apiUrl + 'manage/statistics/getStationReserves',//气站储量统计

    /*我的LNG*/
    getUpdateMyLngUser: ssoUrl + 'manage/system/user/update',//修改用户前数据
    updateMyLngUser: ssoUrl + 'manage/system/user/update/updateUserInfo',//用户更新
    updateMyLngPassword: ssoUrl + 'manage/system/user/update/password',//修改密码
    setMyLngMessage: apiUrl + 'manage/mylng/user/setMessage',//消息设置

    getMessageSettingList: apiUrl + 'manage/mylng/messageSetting/list',//消息设置列表查看
    getMyLngMessageList: apiUrl + 'manage/mylng/message/list',//消息列表
    deleteMyLngMessage: apiUrl + 'manage/mylng/message/delete',//删除消息
    updateMyLngMessage: apiUrl + 'manage/mylng/message/update',//更新消息状态为已读
    getNotReadCount: apiUrl + 'manage/mylng/message/getNotReadCount',//得到未读消息个数

    /*首页*/
    getShowIndexInfo: apiUrl + 'manage/index/showIndexInfo',//获取首页内容

    /*地图信息*/
    getMapInfo: apiUrl + 'manage/amap/station/info',//获取首页内容
    getMapOutline: apiUrl + 'manage/amap/station/outline',//获取首页内容

    /*微信*/
    wxGetToken : wxUrl + '/user/info',
    wxReg : wxUrl + '/user/reg',
    wxJssdk : wxUrl + '/jssdk',

    /*储罐信息*/
    getTankList: apiUrl + 'manage/dictionary/tank/list',//获取储罐信息
    addTank: apiUrl + 'manage/dictionary/tank/add',//新增储罐信息
    updateTank: apiUrl + 'manage/dictionary/tank/update',//修改储罐信息
    deleteTank: apiUrl + 'manage/dictionary/tank/delete',//删除储罐信息

    /*传感器信息*/
    getSensorList: apiUrl + 'manage/dictionary/sensor/list',//获取传感器信息
    getAddSensorList: apiUrl + 'manage/dictionary/sensor/add',//获取传感器新增前数据
    addSensor: apiUrl + 'manage/dictionary/sensor/add',//新增传感器信息
    updateSensor: apiUrl + 'manage/dictionary/sensor/update',//修改传感器信息
    deleteSensor: apiUrl + 'manage/dictionary/sensor/delete',//删除传感器信息
    getAddSensor:apiUrl + 'manage/dictionary/sensor/add',//传感器新增前数据

    /*区域信息*/
    getAreaList: apiUrl + 'manage/dictionary/area/list',//获取区域信息
    getAddAreaList: apiUrl + 'manage/dictionary/area/add',//获取区域新增前数据
    addArea: apiUrl + 'manage/dictionary/area/add',//新增区域信息
    updateArea: apiUrl + 'manage/dictionary/area/update',//修改区域信息
    deleteArea: apiUrl + 'manage/dictionary/area/delete',//删除区域信息
    getAddArea:apiUrl + 'manage/dictionary/area/add',//区域新增前数据

    /*单位信息*/
    getUnitList: apiUrl + 'manage/dictionary/unit/list',//获取单位信息
    addUnit: apiUrl + 'manage/dictionary/unit/add',//新增单位信息
    updateUnit: apiUrl + 'manage/dictionary/unit/update',//修改单位信息
    deleteUnit: apiUrl + 'manage/dictionary/unit/delete',//删除单位信息

    /*图标信息*/
    getDictIconList: apiUrl + 'manage/dictionary/dictIcon/list',//获取图标信息
    addDictIcon: apiUrl + 'manage/dictionary/dictIcon/add',//新增图标信息
    updateDictIcon: apiUrl + 'manage/dictionary/dictIcon/update',//修改图标信息
    deleteDictIcon: apiUrl + 'manage/dictionary/dictIcon/delete',//删除图标信息

    /*消防应急机构*/
    getFireList: apiUrl + 'manage/emergency/fire/list',//获取消防应急机构列表
    addFire: apiUrl + 'manage/emergency/fire/add',//新增消防应急机构
    deleteFire: apiUrl + 'manage/emergency/fire/delete',//删除消防应急机构
    updateFire: apiUrl + 'manage/emergency/fire/update',//修改消防应急机构

    /*医疗应急机构*/
    getHospitalList: apiUrl + 'manage/emergency/hospital/list',//获取医疗应急机构列表
    addHospital: apiUrl + 'manage/emergency/hospital/add',//新增医疗应急机构
    deleteHospital: apiUrl + 'manage/emergency/hospital/delete',//删除医疗应急机构
    updateHospital: apiUrl + 'manage/emergency/hospital/update',//修改医疗应急机构

    /*应急预案管理*/
    getArrplanList: apiUrl + 'manage/emergency/arrplan/list',//获取应急预案管理列表
    addArrplan: apiUrl + 'manage/emergency/arrplan/add',//新增应急预案管理
    deleteArrplan: apiUrl + 'manage/emergency/arrplan/delete',//删除应急预案管理
    updateArrplan: apiUrl + 'manage/emergency/arrplan/update',//修改应急预案管理

    /*应急物资管理*/
    getEmergsupList: apiUrl + 'manage/emergency/emergsup/list',//获取应急物资管理列表
    addEmergsup: apiUrl + 'manage/emergency/emergsup/add',//新增应急物资管理
    deleteEmergsup: apiUrl + 'manage/emergency/emergsup/delete',//删除应急物资管理
    updateEmergsup: apiUrl + 'manage/emergency/emergsup/update',//修改应急物资管理
    getAddEmergsupList: apiUrl + 'manage/emergency/emergsup/add',//获取物资新增前数据

    /*应急避难地管理*/
    getHavenList: apiUrl + 'manage/emergency/haven/list',//获取应急避难地管理列表
    addHaven: apiUrl + 'manage/emergency/haven/add',//新增应急避难地管理
    deleteHaven: apiUrl + 'manage/emergency/haven/delete',//删除应急避难地管理
    updateHaven: apiUrl + 'manage/emergency/haven/update',//修改应急避难地管理

    /*车辆管理*/
    getCarmanagementList: srcUrl + 'manage/delivery/Carmanagement/list',//获取槽车管理列表
    addCarmanagement: srcUrl + 'manage/delivery/Carmanagement/add',//新增槽车管理
    deleteCarmanagement: srcUrl + 'manage/delivery/Carmanagement/delete',//删除槽车管理
    updateCarmanagement: srcUrl + 'manage/delivery/Carmanagement/update',//修改槽车管理
    getAddCarmanagement: srcUrl + 'manage/delivery/Carmanagement/widget',//获取槽车新增前数据
    getAddStationCarmanagement: srcUrl + 'manage/delivery/Carmanagement/add/widget',//获取槽车新增前数据
};

export default path;
