const enumerate  = {};

//站点管理
enumerate.stationType = [
    {
        value: '1',
        label: '加气站'
    },
    {
        value: '2',
        label: '气化站'
    },
    {
        value: '3',
        label: '接收站'
    },
    {
        value: '4',
        label: '液化工厂'
    },
    {
        value: '5',
        label: '调峰站'
    },
    {
        value: '6',
        label: '储气库'
    }
];
enumerate.boundBoxs = [
    {
        value: '0',
        label: '未绑定'
    },
    {
        value: '1',
        label: '已绑定'
    },
];
enumerate.boxStatusList = [
    {
        value: '1',
        label: '离线'
    },
    {
        value: '2',
        label: '在线'
    },
];
enumerate.stationStateList = [
    {
        value: '1',
        label: '正在运营'
    },
    {
        value: '2',
        label: '停运'
    },
    {
        value: '3',
        label: '维修中'
    },
    {
        value: '4',
        label: '关闭'
    },
];
enumerate.stationCertifiedList = [
    {
        value: 0,
        label: '未认证'
    },
    {
        value: 1,
        label: '已认证'
    },
];
enumerate.getStationType = function(type){
    switch (parseInt(type)) {
        case 1: {
            return '加气站'
        }
        case 2: {
            return '气化站'
        }
        case 3: {
            return '接收站'
        }
        case 4: {
            return '液化工厂'
        }
        case 5: {
            return '调峰站'
        }
        case 6: {
            return '储气库'
        }
    }
};
enumerate.getStationState = function(type){
    switch (parseInt(type)) {
        case 1: {
            return '正在运营'
        }
        case 2: {
            return '停运'
        }
        case 3: {
            return '维修中'
        }
        case 4: {
            return '关闭'
        }
    }
};
enumerate.getStationCertified = function(type){
    switch (parseInt(type)) {
        case 0: {
            return '未认证'
        }
        case 1: {
            return '已认证'
        }
    }
};

//企业管理
enumerate.companyTypeList = [
    {
        value: '0',
        label: '气站燃气公司'
    },
    {
        value: '1',
        label: '能源贸易公司'
    },
    {
        value: '2',
        label: '维护公司'
    },
    {
        value: '3',
        label: '用气企业'
    },
    {
        value: '4',
        label: '运输公司'
    },
    {
        value: '5',
        label: '保险公司'
    },
    {
        value: '6',
        label: '政府部门'
    },
    {
        value: '7',
        label: '其它'
    },
];
enumerate.parentCodeList = [
    {
        value:'1',
        label:'中海油',
    },
    {
        value:'2',
        label:'中石化',
    },
    {
        value:'3',
        label:'中石油',
    },
    {
        value:'4',
        label:'新奥',
    },
    {
        value:'5',
        label:'华润',
    },
    {
        value:'6',
        label:'其他',
    },
];
enumerate.getParentCodeList = function(type){
    switch (parseInt(type)) {
        case 1:
            return '中海油';
            break;
        case 2:
            return '中石化';
            break;
        case 3:
            return '中石油';
            break;
        case 4:
            return '新奥';
            break;
        case 5:
            return '华润';
            break;
        case 6:
            return '其他';
            break;
        default:
            break;
    }
};
enumerate.getCompanyType = function(type){
    switch (parseInt(type)) {
        case 0:
            return '气站燃气公司';
            break;
        case 1:
            return '能源贸易公司';
            break;
        case 2:
            return '维护公司';
            break;
        case 3:
            return '用气企业';
            break;
        case 4:
            return '运输公司';
            break;
        case 5:
            return '保险公司';
            break;
        case 6:
            return '政府部门';
            break;
        case 7:
            return '其它';
            break;
        default:
            break;
    }
};

//用户管理
enumerate.getLockedType = function(type){
    switch (parseInt(type)) {
        case 0:
            return '正常';
            break;
        case 1:
            return '锁定';
            break;
        default:
            break;
    }
};
enumerate.getSexType = function(type){
    switch (parseInt(type)) {
        case 0:
            return '女';
            break;
        case 1:
            return '男';
            break;
        default:
            break;
    }
};

//角色管理
enumerate.privilegeList = [
    {
        value:10,
        label:'普通',
    },
    {
        value:40,
        label:'站点',
    },
    {
        value:70,
        label:'公司',
    },
    {
        value:100,
        label:'超级',
    },
];
enumerate.getPrivilegeList = function(type){
    switch (parseInt(type)) {
        case 10:
            return '普通';
            break;
        case 40:
            return '站点';
            break;
        case 70:
            return '公司';
            break;
        case 100:
            return '超级';
            break;
        default:
            break;
    }
};

//权限管理
enumerate.getPermissionType = function(type){
    switch (type) {
        case 1:
            return '目录';
            break;
        case 2:
            return '菜单';
            break;
        case 3:
            return '按钮';
            break;
        default:
            break;
    }
};
enumerate.getPermissionStatus = function(type){
    switch (type) {
        case 0:
            return '异常';
            break;
        case 1:
            return '正常';
            break;
        default:
            break;
    }
};

//lngBox管理
enumerate.lngBoxType = [
    {
        value: '1',
        label: '车辆'
    },
    {
        value: '2',
        label: '气站'
    },
    {
        value: '3',
        label: '船舶'
    },
    {
        value: '4',
        label: '化工'
    },
    {
        value: '5',
        label: '管道'
    }
];
enumerate.lngBoxStatus = [
    {
        value: '1',
        label: '离线'
    },
    {
        value: '2',
        label: '在线'
    }
];
enumerate.updateResult = [
    {
        value: '1',
        label: '成功'
    },
    {
        value: '2',
        label: '失败'
    },
    {
        value: '3',
        label: '更新中'
    }
];
enumerate.isNegativeList = [
    {
        value: '1',
        label: '否'
    },
    {
        value: '2',
        label: '是'
    },
];
enumerate.isOverdraftList = [
    {
        value: '1',
        label: '否'
    },
    {
        value: '2',
        label: '是'
    },
];
enumerate.whetherList = [
    {
        value: '0',
        label: '不显示'
    },
    {
        value: '1',
        label: '显示'
    }
];
enumerate.getLngBoxType = function(type){
    switch (parseInt(type)) {
        case 1:
            return '车辆';
            break;
        case 2:
            return '气站';
            break;
        case 3:
            return '船舶';
            break;
        case 4:
            return '化工';
            break;
        case 5:
            return '管道';
            break;
        default:
            break;
    }
};
enumerate.getLngBoxStatus = function(type){
    switch (type) {
        case 1:
            return '离线';
            break;
        case 2:
            return '在线';
            break;
        default:
            break;
    }
};
enumerate.getIsNegative = function(type){
    switch (type) {
        case 1:
            return '否';
            break;
        case 2:
            return '是';
            break;
        default:
            break;
    }
};
enumerate.getIsOverdraft = function(type){
    switch (type) {
        case 1:
            return '否';
            break;
        case 2:
            return '是';
            break;
        default:
            break;
    }
};
enumerate.getUpdateResult = function(type){
    switch (type) {
        case 1:
            return '成功';
            break;
        case 2:
            return '失败';
            break;
        case 3:
            return '更新中';
            break;
        default:
            break;
    }
};
enumerate.getdisplay = function(type){
    switch(parseInt(type)){
        case 0:
            return '不显示';
            break;
        case 1:
            return '显示';
            break;
        default:
            break;
    }
};

//触发器
enumerate.triggerSituationList = [
    {
        value: '1',
        label: '数值高于X'
    },
    {
        value: '2',
        label: '数值低于Y'
    },
    {
        value: '3',
        label: '数值高于X低于Y'
    },
    {
        value: '4',
        label: '数值高于X低于Y超过M分钟'
    },
    {
        value: '5',
        label: '数值高于X报警低于Y恢复'
    },
    {
        value: '6',
        label: '数值在X于Y之间'
    },
    {
        value: '7',
        label: '数值超过M分钟高于X'
    },
    {
        value: '8',
        label: '数值超过M分钟低于Y'
    },
    {
        value: '9',
        label: '传感器断开'
    },
    {
        value: '10',
        label: '超过M分钟断开'
    },
    {
        value: '11',
        label: '开关开启'
    },
    {
        value: '12',
        label: '开关关闭'
    }
];
enumerate.alarmMethodList = [
    {
        value: '1',
        label: '短信通知'
    },
    {
        value: '2',
        label: 'APP通知'
    },
    {
        value: '3',
        label: '微信通知'
    }
];
enumerate.getTriggerSituation = function(type){
    switch (type){
        case 1:
            return '数值高于X';
            break;
        case 2:
            return '数值低于Y';
            break;
        case 3:
            return '数值高于X低于Y';
            break;
        case 4:
            return '数值高于X低于Y超过M分钟';
            break;
        case 5:
            return '数值高于X报警低于Y恢复';
            break;
        case 6:
            return '数值在X于Y之间';
            break;
        case 7:
            return '数值超过M分钟高于X';
            break;
        case 8:
            return '数值超过M分钟低于Y';
            break;
        case 9:
            return '传感器断开';
            break;
        case 10:
            return '超过M分钟断开';
            break;
        case 11:
            return '开关开启';
            break;
        case 12:
            return '开关关闭';
            break;
        default:
            break;

    }
};
enumerate.getAlarmMethod = function(type){
    switch (type){
        case 1:
            return '短信通知';
            break;
        case 2:
            return 'APP通知';
            break;
        case 3:
            return '微信通知';
            break;
        default:
            break;
    }
};

//车辆
enumerate.vehicleTypeList = [
    {
        value: '0',
        label: '槽车'
    },
    {
        value: '1',
        label: '公交车'
    },
    {
        value: '2',
        label: '重卡'
    },
    {
        value: '3',
        label: '出租车'
    }
];

//数据字典
enumerate.placementList = [
    {
        value: '1',
        label: '卧式'
    },
    {
        value: '2',
        label: '立式'
    },
];
enumerate.liquidList = [
    {
        value: '1',
        label: 'LNG'
    },
    {
        value: '2',
        label: '液氮'
    },
    {
        value: '3',
        label: '液氩'
    },
    {
        value: '4',
        label: '液氧'
    }
];
enumerate.sensorTypeList = [
    {
        value: '1',
        label: 'AI'
    },
    {
        value: '2',
        label: 'RS485'
    },
    {
        value: '3',
        label: 'RS232'
    },
    {
        value: '4',
        label: 'DI'
    },
    {
        value: '5',
        label: 'DO'
    }
];
enumerate.isChargingList = [
    {
        value: '0',
        label: '不计费'
    },
    {
        value: '1',
        label: '计费'
    }
];
enumerate.chartTypeList = [
    {
        value: '1',
        label: '线图'
    },
    {
        value: '2',
        label: '柱状图'
    }
];
enumerate.regionTypeList = [
    {
        value: '1',
        label: '储罐区'
    },
    {
        value: '2',
        label: '调压计量区'
    },
    {
        value: '3',
        label: '气化器区'
    },
    {
        value: '4',
        label: '泵撬区'
    },
    {
        value: '5',
        label: '卸车区'
    },
    {
        value: '6',
        label: '其他'
    }
];
enumerate.iconTypeList = [
    {
        value: '1',
        label: '区域'
    },
    {
        value: '2',
        label: '传感器'
    }
];
enumerate.getPlacementType = function(type){
    switch (type){
        case 1:
            return '卧式';
            break;
        case 2:
            return '立式';
            break;
        default:
            break;
    }
};
enumerate.getLiquidTypeType = function(type){
    switch (type){
        case 1:
            return 'LNG';
            break;
        case 2:
            return '液氮';
            break;
        case 3:
            return '液氩';
            break;
        case 4:
            return '液氧';
            break;
        default:
            break;
    }
};
enumerate.getSensorTypeList = function(type){
    switch (parseInt(type)){
        case 1:
            return 'AI';
            break;
        case 2:
            return 'RS485';
            break;
        case 3:
            return 'RS232';
            break;
        case 4:
            return 'DI';
            break;
        case 5:
            return 'DO';
            break;
        default:
            break;
    }
};
enumerate.getChartType = function(type){
    switch (parseInt(type)){
        case 1:
            return '线图';
            break;
        case 2:
            return '柱状图';
            break;
        default:
            break;
    }
};
enumerate.getIsCharging = function(type){
    switch (parseInt(type)){
        case 0:
            return '不计费';
            break;
        case 1:
            return '计费';
            break;
        default:
            break;
    }
};
enumerate.getRegionTypeList = function(type){
    switch (parseInt(type)){
        case 1:
            return '储罐区';
            break;
        case 2:
            return '调压计量区';
            break;
        case 3:
            return '气化器区';
            break;
        case 4:
            return '泵撬区';
            break;
        case 5:
            return '卸车区';
            break;
        case 6:
            return '其他';
            break;
        default:
            break;
    }
};
enumerate.getIconTypeList = function(type){
    switch (parseInt(type)){
        case 1:
            return '区域';
            break;
        case 2:
            return '传感器';
            break;
        default:
            break;
    }
};

//个人中心
enumerate.messageTypeList = [
    {
        id: '',
        messageType: 0,
        receive: '1'
    },
    {
        id: '',
        messageType: 1,
        receive: '1'
    },
    {
        id: '',
        messageType: 2,
        receive: '1'
    },
    {
        id: '',
        messageType: 3,
        receive: '1'
    },
    {
        id: '',
        messageType: 4,
        receive: '1'
    }
];
enumerate.readStateList = [
    {
        value: '0',
        label: '已读'
    },
    {
        value: '1',
        label: '未读'
    }
];
enumerate.getMessageTypeName = function(type){
    switch (type) {
        case 0:
            return '订单提醒';
            break;
        case 1:
            return '巡检提醒';
            break;
        case 2:
            return '故障提醒';
            break;
        case 3:
            return '设备提醒';
            break;
        case 4:
            return '报警提醒';
            break;
        default:
            return '未知';
    }
};

//监控预警
enumerate.classicList = [
    {
        value: '0',
        label: '白班'
    },
    {
        value: '1',
        label: '中班'
    },
    {
        value: '2',
        label: '夜班'
    }
];
enumerate.indexList = [
    {
        value: '第一次',
        label: '第一次'
    },
    {
        value: '第二次',
        label: '第二次'
    },
    {
        value: '第三次',
        label: '第三次'
    },
    {
        value: '4',
        label: '第四次'
    }
];
enumerate.takeClassicType = [
    {
        value: '0',
        label: '早班'
    },
    {
        value: '1',
        label: '中班'
    },
    {
        value: '2',
        label: '晚班'
    }
];
enumerate.getAlarmStatus = function(type){
    switch (parseInt(type)) {
        case 0:
            return '待处理';
            break;
        case 1:
            return '已处理';
            break;
        default:
            break;
    }
};

//应急指挥
enumerate.arrTypeList = [
    {
        value: '0',
        label: '综合预案'
    },
    {
        value: '1',
        label: '专项预案'
    },
    {
        value: '2',
        label: '现场预案'
    },
    {
        value: '3',
        label: '其他预案'
    }
];
enumerate.arrGradeList = [
    {
        value: '0',
        label: '省部级'
    },
    {
        value: '1',
        label: '地市级'
    },
    {
        value: '2',
        label: '县级'
    },
    {
        value: '3',
        label: '公司级'
    },
    {
        value: '4',
        label: '厂级'
    }
];
enumerate.havenTypeList = [
    {
        value:'0',
        label:'室外'
    },
    {
        value:'1',
        label:'室内'
    }
];
enumerate.medicalGradeList = [
    {
        value: '1',
        label: '一级甲等'
    },
    {
        value: '2',
        label: '一级乙等'
    },
    {
        value: '3',
        label: '一级丙等'
    },
    {
        value: '4',
        label: '二级甲等'
    },
    {
        value: '5',
        label: '二级乙等'
    },
    {
        value: '6',
        label: '二级丙等'
    },
    {
        value: '7',
        label: '三级甲等'
    },
    {
        value: '8',
        label: '三级乙等'
    },
    {
        value: '9',
        label: '三级丙等'
    }
];

export default enumerate;
